import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Fdata } from '../shared/fdata-model';
import { UserService } from '../shared/user-service';
import { ErrorMsg } from '../shared/errormsgs'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {
  commForm: FormGroup;
  loading =false;
  // submitted: boolean = false;
  emptyfield;
  minimumCharacters;
  mini6characters;
  validvalue;
  
  constructor( private formBuilder: FormBuilder, private router: Router, private uservice: UserService, private errormsg: ErrorMsg) {
      this.emptyfield = this.errormsg.errmsg().nofield;
      this.minimumCharacters = this.errormsg.errmsg().minchar3;
      this.validvalue = this.errormsg.errmsg().validinput;
      this.mini6characters = this.errormsg.errmsg().minchar6;

      this.commForm = this.formBuilder.group({
        name: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
        username: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
        contact: ['',  Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        address: ['', Validators.compose([Validators.required])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        confirmpassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
      });
   }

  ngOnInit() {

  }

  onSubmit(){
    console.log(this.commForm);
    this.uservice.addFdata(this.commForm.value);
    this.router.navigate(['/users']);
  }


  set(){
    
      this.commForm.reset();
    
  }

}
