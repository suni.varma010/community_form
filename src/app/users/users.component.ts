import { Component, OnInit } from '@angular/core';

import { Fdata } from '../shared/fdata-model';
import { UserService } from '../shared/user-service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  udata: Fdata[];
  constructor(private uservice: UserService, private authService: AuthService) { }

  ngOnInit() {
    this.udata = this.uservice.getFndata();
    this.uservice.datachange
      .subscribe(
        (userdetail: Fdata[]) => {
          this.udata = userdetail;
        }
      );
  }
}

