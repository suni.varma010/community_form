import { Fdata } from './fdata-model';
import { Subject } from 'rxjs';

export class UserService{
  datachange = new Subject<Fdata[]>();
  private fndata: Fdata[] = [];

  getFndata(){
    return this.fndata.slice();
  }

  addFdata(fdata: Fdata){
    this.fndata.push(fdata);
    this.datachange.next(this.fndata.slice());
  }

  // addUdata(udata: Fdata[]){
  //     this.fndata.push(...udata);
  //     this.datachange.emit(this.fndata.slice());
  // }
}
