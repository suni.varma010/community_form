export class Fdata{

   constructor( 
    public name?: string,
    public username?: string,
    public contact?: number,
    public email?: string,
    public address?: string,
    public password?: string,
    public confirmpwd?: string){}

}
