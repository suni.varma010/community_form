import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ErrorMsg } from '../shared/errormsgs';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  logForm: FormGroup;
  emptyfield;
  validvalue;
  mini6characters;
  constructor(private errormsg: ErrorMsg, public authService: AuthService) { 
    this.emptyfield = this.errormsg.errmsg().nofield;
    this.validvalue = this.errormsg.errmsg().validinput;
    this.mini6characters = this.errormsg.errmsg().minchar6;
   }

  ngOnInit() {
    this.logForm = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      password: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)]))
    })
  }
 onSubmit(){

 }
}
