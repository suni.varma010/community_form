import { Mcq } from './mcqmodel';
import { EventEmitter } from '@angular/core';

export class McqService{
    listchange = new EventEmitter<Mcq[]>();
    private mcqlist: Mcq[] = [];

    getMcq(){
        return this.mcqlist.slice();
    }

    addMcq(quest: Mcq){
        this.mcqlist.push(quest);
        this.listchange.emit(this.mcqlist.slice());
    }
}