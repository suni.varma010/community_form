import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { McqService } from './mcqservice';

@Component({
  selector: 'app-mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.scss']
})
export class McqComponent implements OnInit {
  queForm: FormGroup;
  submitted: boolean = false;
  constructor(private mcqservice: McqService, private router: Router) { }

  ngOnInit() {
    this.queForm = new FormGroup({
      question: new FormControl('', Validators.required)
    });
  }
  addQuestion(){
    this.submitted = true;
    this.mcqservice.addMcq(this.queForm.value);
    this.router.navigate(['/mcqlist']);
  }
}
