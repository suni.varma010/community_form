import { Component, OnInit } from '@angular/core';

import { Mcq } from '../mcqmodel';
import { McqService } from '../mcqservice';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-mcqlist',
  templateUrl: './mcqlist.component.html',
  styleUrls: ['./mcqlist.component.scss']
})
export class McqlistComponent implements OnInit {
mcquestion: Mcq[];
ansForm: FormGroup;
answers: Array<Mcq>;

  constructor(private mcqservice: McqService) {
    this.answers = [];
   }

  addAns(){
    let ans = new Mcq(this.ansForm.value.answer);
    this.answers.push(ans);
  }

  ngOnInit() {
    this.mcquestion = this.mcqservice.getMcq();
    this.mcqservice.listchange
     .subscribe(
       (questions: Mcq[]) => {
          this.mcquestion = questions;
       });

    this.ansForm = new FormGroup({
      answer: new FormControl('', Validators.required)
    });
  }

}
