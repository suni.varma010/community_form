import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McqlistComponent } from './mcqlist.component';

describe('McqlistComponent', () => {
  let component: McqlistComponent;
  let fixture: ComponentFixture<McqlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McqlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McqlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
