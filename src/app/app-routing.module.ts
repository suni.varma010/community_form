import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { McqComponent } from './mcq/mcq.component';
import { McqlistComponent } from './mcq/mcqlist/mcqlist.component';
import { AuthGaurd } from './shared/auth-gaurd.service';

const routes: Routes = [
  { path: '', pathMatch:'full', redirectTo: '/register' },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent},
  { path: 'mcq', component: McqComponent, canActivate: [AuthGaurd]},
  { path: 'mcqlist', component: McqlistComponent , canActivate: [AuthGaurd]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  static components = [RegisterComponent, LoginComponent, UsersComponent, McqComponent, McqlistComponent];
}
